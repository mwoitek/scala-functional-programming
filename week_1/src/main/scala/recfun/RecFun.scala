package recfun

import scala.annotation.tailrec

object RecFun extends RecFunInterface:

  def main(args: Array[String]): Unit =
    println("Pascal's Triangle")
    for row <- 0 to 10 do
      for col <- 0 to row do print(s"${pascal(col, row)} ")
      println()

  // Exercise 1
  def pascal(c: Int, r: Int): Int =
    def multiply(c: Int, r: Int): Int =
      @tailrec
      def loop(idx: Int, acc: Int): Int = if (idx > c) acc else loop(idx + 1, acc * (r + 1 - idx))
      loop(1, 1)

    def divide(c: Int, product: Int): Int =
      @tailrec
      def loop(idx: Int, acc: Int): Int = if (idx > c) acc else loop(idx + 1, acc / idx)
      loop(2, product)

    if (r >= 2 * c) divide(c, multiply(c, r)) else divide(r - c, multiply(r - c, r))

  // Exercise 2
  def balance(chars: List[Char]): Boolean =
    @tailrec
    def loop(chars: List[Char], stack: List[Char]): Boolean =
      if (chars.isEmpty) stack.isEmpty
      else if (chars.head == '(') loop(chars.tail, '(' +: stack)
      else if (chars.head == ')') (if (stack.isEmpty) false else loop(chars.tail, stack.tail))
      else loop(chars.tail, stack)
    loop(chars, List())

  // Exercise 3
  def countChange(money: Int, coins: List[Int]): Int =
    if (money == 0) 1
    else if (money < 0 || coins.isEmpty) 0
    else countChange(money - coins.head, coins) + countChange(money, coins.tail)
