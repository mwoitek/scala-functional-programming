#!/bin/sh
export PATH="/usr/lib/jvm/java-11-openjdk/bin:${PATH}"
exec "${HOME}/.local/share/coursier/bin/sbt" "$@"
